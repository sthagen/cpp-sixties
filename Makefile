TARGET_EXEC ?= a.out

BUILD_DIR ?= ./build
SRC_DIRS ?= ./src

SRCS := $(shell find $(SRC_DIRS) -name *.cpp -or -name *.c -or -name *.s)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

INC_DIRS := $(shell find $(SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CPPFLAGS ?= $(INC_FLAGS) -MMD -MP -O2 -pedantic -Wall -Wextra -Weffc++ -Werror -Wno-error=effc++ -std=c++11
CC = g++

$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CC) $(OBJS) -o $@ $(LDFLAGS)

# assembly
$(BUILD_DIR)/%.s.o: %.s
	$(MKDIR_P) $(dir $@)
	$(AS) $(ASFLAGS) -c $< -o $@

# c source
$(BUILD_DIR)/%.c.o: %.c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

# c++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@


.PHONY: clean

all: read_matrix_pure read_fixed_pure read_fixed_unix readline_unix

read_matrix_pure: $(SRC_DIRS)/read_matrix_pure.cpp
	$(MKDIR_P) $(BUILD_DIR)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< -o $(BUILD_DIR)/$@

read_fixed_pure: $(SRC_DIRS)/read_fixed_pure.cpp
	$(MKDIR_P) $(BUILD_DIR)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< -o $(BUILD_DIR)/$@

read_fixed_unix: $(SRC_DIRS)/read_fixed_unix.cpp
	$(MKDIR_P) $(BUILD_DIR)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< -o $(BUILD_DIR)/$@

readline_unix: $(SRC_DIRS)/readline_unix.cpp
	$(MKDIR_P) $(BUILD_DIR)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< -o $(BUILD_DIR)/$@

corpus:
	data/derive_corpus.sh

test: all
	./run_tests.sh

lint:
	shellcheck run_tests.sh data/*.sh
	cppcheck src/*.cpp

clean:
	$(RM) -r $(BUILD_DIR)

distclean: clean
	data/minimize_corpus.sh

-include $(DEPS)

MKDIR_P ?= mkdir -p
