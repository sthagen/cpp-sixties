# cpp-sixties

Parsing of fixed width text databases derived in the Seventies from the Sixties’ spirit of the previous century.

Strategies to be implemented:

* read chunks of multiple complete records
* identify records via three level decision tree targeting fixed cells per record
* work on views as long as possible
* cast numeric text representations directly from bytes to numerical fields
* provide lazy API where client code may request post processing and search and fields published as index
* experiment with string_view to replace const char* where possible to please the reader