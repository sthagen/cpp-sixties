#! /bin/bash
# Remove the derived corpus data and keep the seed files
pushd "$(dirname "${BASH_SOURCE[0]}")" || exit
s="a"
pure="_pure.fwd"
unix="_unix.fwd" 
printf "Minimizing corpora, but keeping the seed files %s and %s\n" "${s}${pure}" "${s}${unix}"
for t in b c d e f g h i j k l m n o p q r s t
do 
    for species in "${pure}" "${unix}"
    do
        rm -f "${t}${species}"
    done
done
printf "Remaining minimal corpus fles:\n"
ls -l -- *.fwd
printf "Done.\n"
popd || exit
