#! /bin/bash
# Derive the unix line ending and pure corpus data from the seed files
pushd "$(dirname "${BASH_SOURCE[0]}")" || exit
s="a"
pure="_pure.fwd"
unix="_unix.fwd" 
printf "Deriving corpora from the seed files %s and %s\n" "${s}${pure}" "${s}${unix}"
for t in b c d e f g h i j k l m n o p q r s t
do 
    for species in "${pure}" "${unix}"
    do
        cat "${s}${species}" "${s}${species}" > "${t}${species}"
        if [ "${species}" == "${unix}" ]
        then
            printf "Derived unix linefeed corpus %s with %s lines\n" "${t}${species}" "$(wc -l "${t}${species}")"
        else
            printf "Derived pure record corpus %s with %s bytes\n" "${t}${species}" "$(wc -c "${t}${species}")"
        fi
    done
    s="${t}"
done
printf "Showing the listing of the corpus fles:\n"
ls -l -- *.fwd
printf "Done.\n"
popd || exit
