#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

const size_t RECORD_BYTES = 132;
const size_t BLOCK_BYTES = 2 << 15;
const size_t CHUNKS_MAX = BLOCK_BYTES / RECORD_BYTES;
const size_t BUFFER_BYTES = CHUNKS_MAX * RECORD_BYTES;

const bool DEBUG = (std::getenv("DEBUG_60S")) ? true : false;

struct p_a_record_t {
    char a[1];
    char b[3];
    char c[1];
    char d[1];
    char e[4];
    char f[2];
    char g[1];
    char h[3];

    char j[2];
    char k[3];
    char l[1];
    char m[5];
    char n[3];
    char o[1];
    char p[1];
    char q[9];
    char r[10];
    char s[5];
    char t[5];
    char u[3];
    char v[4];
    char w[2];
    char x[5];
    char y[5];
    char z[1];

    char aa[3];
    char ab[1];
    char ac[1];
    char ad[3];
    char ae[4];
    char af[30];
    char ag[5];
    char ah[4];
};

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cerr << "Usage: " << __FILE__ << " database_path\n";
        exit(1);
    }
    const char *database_path{argv[1]};
    std::FILE *fwd = fopen(database_path, "r");
    if (fwd == nullptr) {
        std::cerr << "Error opening file " << database_path << "\n";
        exit(1);
    }
    char buf[BUFFER_BYTES];
    p_a_record_t record{};

    size_t records = 0;
    std::chrono::time_point<std::chrono::steady_clock> start, end;
    start = std::chrono::steady_clock::now();

    while (true) {
        size_t b = std::fread(&buf[0], sizeof buf[0], BUFFER_BYTES, fwd);
        if (b < RECORD_BYTES) break;

        for (size_t n = 0; n < b; n += RECORD_BYTES) {
            memcpy(&record, &buf[n], sizeof record);
            ++records;
            if (DEBUG) {
                std::cout << "at " << records << " read (\n";
                std::cout << std::string(record.a, 1);
                std::cout << std::string(record.b, 3);
                std::cout << std::string(record.c, 1);
                std::cout << std::string(record.d, 1);
                std::cout << std::string(record.e, 4);
                std::cout << std::string(record.f, 2);
                std::cout << std::string(record.g, 1);
                std::cout << std::string(record.h, 3);

                std::cout << std::string(record.j, 2);
                std::cout << std::string(record.k, 3);
                std::cout << std::string(record.l, 1);
                std::cout << std::string(record.m, 5);
                std::cout << std::string(record.n, 3);
                std::cout << std::string(record.o, 1);
                std::cout << std::string(record.p, 1);
                std::cout << std::string(record.q, 9);
                std::cout << std::string(record.r, 10);
                std::cout << std::string(record.s, 5);
                std::cout << std::string(record.t, 5);
                std::cout << std::string(record.u, 3);
                std::cout << std::string(record.v, 4);
                std::cout << std::string(record.w, 2);
                std::cout << std::string(record.x, 5);
                std::cout << std::string(record.y, 5);
                std::cout << std::string(record.z, 1);

                std::cout << std::string(record.aa, 3);
                std::cout << std::string(record.ab, 1);
                std::cout << std::string(record.ac, 1);
                std::cout << std::string(record.ad, 3);
                std::cout << std::string(record.ae, 4);
                std::cout << std::string(record.af, 30);
                std::cout << std::string(record.ag, 5);
                std::cout << std::string(record.ah, 4);

                std::cout << "\n)\n";
            }
        }
    }

    end = std::chrono::steady_clock::now();
    size_t dt_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

    fclose(fwd);
    std::cout  << records << "," << dt_ms << "\n";
}
