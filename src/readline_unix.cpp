#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>

const size_t RECORD_BYTES = 132;

const bool DEBUG = (std::getenv("DEBUG_60S")) ? true : false;
const bool INDEX_ACCESS = (std::getenv("INDEX_ACCESS_60S")) ? true : false;
const bool PLUS_EQUAL = (std::getenv("PLUS_EQUAL_60S")) ? true : false;
const bool PUSH_BACK = (std::getenv("PUSH_BACK_60S")) ? true : false;
const bool SUBSTRING = (std::getenv("SUBSTRNG_60S")) ? true : false;

const char CHR = '~';

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cerr << "Usage: " << __FILE__ << " database_path\n";
        exit(1);
    }
    const std::string database_path{argv[1]};
    std::ifstream fwd(database_path);
    if (!fwd.is_open()) {
        std::cerr << "Error opening file " << database_path << "\n";
        exit(1);
    }
    std::string line;
    size_t records = 0;
    std::chrono::time_point<std::chrono::steady_clock> start, end;
    start = std::chrono::steady_clock::now();

    std::string a(1, CHR);
    std::string b(3, CHR);
    std::string c(1, CHR);
    std::string d(1, CHR);
    std::string e(4, CHR);
    std::string f(2, CHR);
    std::string g(1, CHR);
    std::string h(3, CHR);

    std::string j(2, CHR);
    std::string k(3, CHR);
    std::string l(1, CHR);
    std::string m(5, CHR);
    std::string n(3, CHR);
    std::string o(1, CHR);
    std::string p(1, CHR);
    std::string q(9, CHR);
    std::string r(10, CHR);
    std::string s(5, CHR);
    std::string t(5, CHR);
    std::string u(3, CHR);
    std::string v(4, CHR);
    std::string w(2, CHR);
    std::string x(5, CHR);
    std::string y(5, CHR);
    std::string z(1, CHR);

    std::string aa(3, CHR);
    std::string ab(1, CHR);
    std::string ac(1, CHR);
    std::string ad(3, CHR);
    std::string ae(4, CHR);
    std::string af(30, CHR);
    std::string ag(5, CHR);
    std::string ah(4, CHR);
    while (std::getline(fwd, line, '\n')) {
        if (fwd.fail() || fwd.bad()) {
            std::cerr << "Error reading line " << records + 1 << "\n";
            exit(1);
        }
        ++records;
        if (INDEX_ACCESS) {
            if (line.size() < RECORD_BYTES) {
                std::cerr << "DEFECT record(" << line << ") length < record_size(" << RECORD_BYTES << """\n";
                exit(1);
            }
            int ii = -1;
            ii = -1; for(auto i =   0; i <   1; i++) a[++ii] = line[i];
            ii = -1; for(auto i =   1; i <   4; i++) b[++ii] = line[i];
            ii = -1; for(auto i =   4; i <   5; i++) c[++ii] = line[i];
            ii = -1; for(auto i =   5; i <   6; i++) d[++ii] = line[i];
            ii = -1; for(auto i =   6; i <  10; i++) e[++ii] = line[i];
            ii = -1; for(auto i =  10; i <  12; i++) f[++ii] = line[i];
            ii = -1; for(auto i =  12; i <  13; i++) g[++ii] = line[i];
            ii = -1; for(auto i =  16; i <  16; i++) h[++ii] = line[i];

            ii = -1; for(auto i =  16; i <  18; i++) j[++ii] = line[i];
            ii = -1; for(auto i =  18; i <  21; i++) k[++ii] = line[i];
            ii = -1; for(auto i =  21; i <  22; i++) l[++ii] = line[i];
            ii = -1; for(auto i =  22; i <  27; i++) m[++ii] = line[i];
            ii = -1; for(auto i =  27; i <  30; i++) n[++ii] = line[i];
            ii = -1; for(auto i =  30; i <  31; i++) o[++ii] = line[i];
            ii = -1; for(auto i =  31; i <  32; i++) p[++ii] = line[i];
            ii = -1; for(auto i =  32; i <  41; i++) q[++ii] = line[i];
            ii = -1; for(auto i =  41; i <  51; i++) r[++ii] = line[i];
            ii = -1; for(auto i =  51; i <  56; i++) s[++ii] = line[i];
            ii = -1; for(auto i =  56; i <  61; i++) t[++ii] = line[i];
            ii = -1; for(auto i =  61; i <  64; i++) u[++ii] = line[i];
            ii = -1; for(auto i =  64; i <  68; i++) v[++ii] = line[i];
            ii = -1; for(auto i =  68; i <  70; i++) w[++ii] = line[i];
            ii = -1; for(auto i =  70; i <  75; i++) x[++ii] = line[i];
            ii = -1; for(auto i =  75; i <  80; i++) y[++ii] = line[i];
            ii = -1; for(auto i =  80; i <  81; i++) z[++ii] = line[i];

            ii = -1; for(auto i =  81; i <  84; i++) aa[++ii] = line[i];
            ii = -1; for(auto i =  84; i <  85; i++) ab[++ii] = line[i];
            ii = -1; for(auto i =  85; i <  86; i++) ac[++ii] = line[i];
            ii = -1; for(auto i =  86; i <  89; i++) ad[++ii] = line[i];
            ii = -1; for(auto i =  89; i <  93; i++) ae[++ii] = line[i];
            ii = -1; for(auto i =  93; i < 123; i++) af[++ii] = line[i];
            ii = -1; for(auto i = 123; i < 128; i++) ag[++ii] = line[i];
            ii = -1; for(auto i = 128; i < 132; i++) ah[++ii] = line[i];

            line = a + b + c + d + e + f + g + h +   j + k + l + m + n + o + p + q + r + s + t + u + v + w + x + y + z + aa + ab + ac + ad + ae + af + ag + ah;
        } else if (PLUS_EQUAL) {
            if (line.size() < RECORD_BYTES) {
                std::cerr << "DEFECT record(" << line << ") length < record_size(" << RECORD_BYTES << """\n";
                exit(1);
            }
            a.clear(); for(auto i =   0; i <   1; i++) a += line[i];
            b.clear(); for(auto i =   1; i <   4; i++) b += line[i];
            c.clear(); for(auto i =   4; i <   5; i++) c += line[i];
            d.clear(); for(auto i =   5; i <   6; i++) d += line[i];
            e.clear(); for(auto i =   6; i <  10; i++) e += line[i];
            f.clear(); for(auto i =  10; i <  12; i++) f += line[i];
            g.clear(); for(auto i =  12; i <  13; i++) g += line[i];
            h.clear(); for(auto i =  16; i <  16; i++) h += line[i];

            j.clear(); for(auto i =  16; i <  18; i++) j += line[i];
            k.clear(); for(auto i =  18; i <  21; i++) k += line[i];
            l.clear(); for(auto i =  21; i <  22; i++) l += line[i];
            m.clear(); for(auto i =  22; i <  27; i++) m += line[i];
            n.clear(); for(auto i =  27; i <  30; i++) n += line[i];
            o.clear(); for(auto i =  30; i <  31; i++) o += line[i];
            p.clear(); for(auto i =  31; i <  32; i++) p += line[i];
            q.clear(); for(auto i =  32; i <  41; i++) q += line[i];
            r.clear(); for(auto i =  41; i <  51; i++) r += line[i];
            s.clear(); for(auto i =  51; i <  56; i++) s += line[i];
            t.clear(); for(auto i =  56; i <  61; i++) t += line[i];
            u.clear(); for(auto i =  61; i <  64; i++) u += line[i];
            v.clear(); for(auto i =  64; i <  68; i++) v += line[i];
            w.clear(); for(auto i =  68; i <  70; i++) w += line[i];
            x.clear(); for(auto i =  70; i <  75; i++) x += line[i];
            y.clear(); for(auto i =  75; i <  80; i++) y += line[i];
            z.clear(); for(auto i =  80; i <  81; i++) z += line[i];

            aa.clear(); for(auto i =  81; i <  84; i++) aa += line[i];
            ab.clear(); for(auto i =  84; i <  85; i++) ab += line[i];
            ac.clear(); for(auto i =  85; i <  86; i++) ac += line[i];
            ad.clear(); for(auto i =  86; i <  89; i++) ad += line[i];
            ae.clear(); for(auto i =  89; i <  93; i++) ae += line[i];
            af.clear(); for(auto i =  93; i < 123; i++) af += line[i];
            ag.clear(); for(auto i = 123; i < 128; i++) ag += line[i];
            ah.clear(); for(auto i = 128; i < 132; i++) ah += line[i];

            line = a + b + c + d + e + f + g + h +   j + k + l + m + n + o + p + q + r + s + t + u + v + w + x + y + z + aa + ab + ac + ad + ae + af + ag + ah;

            line = a + b + c + d + e + f + g + h +   j + k + l + m + n + o + p + q + r + s + t + u + v + w + x + y + z + aa + ab + ac + ad + ae + af + ag + ah;
        } else if (PUSH_BACK) {
            if (line.size() < RECORD_BYTES) {
                std::cerr << "DEFECT record(" << line << ") length < record_size(" << RECORD_BYTES << "\n";
                exit(1);
            }
            a.clear(); for(auto i =   0; i <   1; i++) a.push_back(line[i]);
            b.clear(); for(auto i =   1; i <   4; i++) b.push_back(line[i]);
            c.clear(); for(auto i =   4; i <   5; i++) c.push_back(line[i]);
            d.clear(); for(auto i =   5; i <   6; i++) d.push_back(line[i]);
            e.clear(); for(auto i =   6; i <  10; i++) e.push_back(line[i]);
            f.clear(); for(auto i =  10; i <  12; i++) f.push_back(line[i]);
            g.clear(); for(auto i =  12; i <  13; i++) g.push_back(line[i]);
            h.clear(); for(auto i =  16; i <  16; i++) h.push_back(line[i]);

            j.clear(); for(auto i =  16; i <  18; i++) j.push_back(line[i]);
            k.clear(); for(auto i =  18; i <  21; i++) k.push_back(line[i]);
            l.clear(); for(auto i =  21; i <  22; i++) l.push_back(line[i]);
            m.clear(); for(auto i =  22; i <  27; i++) m.push_back(line[i]);
            n.clear(); for(auto i =  27; i <  30; i++) n.push_back(line[i]);
            o.clear(); for(auto i =  30; i <  31; i++) o.push_back(line[i]);
            p.clear(); for(auto i =  31; i <  32; i++) p.push_back(line[i]);
            q.clear(); for(auto i =  32; i <  41; i++) q.push_back(line[i]);
            r.clear(); for(auto i =  41; i <  51; i++) r.push_back(line[i]);
            s.clear(); for(auto i =  51; i <  56; i++) s.push_back(line[i]);
            t.clear(); for(auto i =  56; i <  61; i++) t.push_back(line[i]);
            u.clear(); for(auto i =  61; i <  64; i++) u.push_back(line[i]);
            v.clear(); for(auto i =  64; i <  68; i++) v.push_back(line[i]);
            w.clear(); for(auto i =  68; i <  70; i++) w.push_back(line[i]);
            x.clear(); for(auto i =  70; i <  75; i++) x.push_back(line[i]);
            y.clear(); for(auto i =  75; i <  80; i++) y.push_back(line[i]);
            z.clear(); for(auto i =  80; i <  81; i++) z.push_back(line[i]);

            aa.clear(); for(auto i =  81; i <  84; i++) aa.push_back(line[i]);
            ab.clear(); for(auto i =  84; i <  85; i++) ab.push_back(line[i]);
            ac.clear(); for(auto i =  85; i <  86; i++) ac.push_back(line[i]);
            ad.clear(); for(auto i =  86; i <  89; i++) ad.push_back(line[i]);
            ae.clear(); for(auto i =  89; i <  93; i++) ae.push_back(line[i]);
            af.clear(); for(auto i =  93; i < 123; i++) af.push_back(line[i]);
            ag.clear(); for(auto i = 123; i < 128; i++) ag.push_back(line[i]);
            ah.clear(); for(auto i = 128; i < 132; i++) ah.push_back(line[i]);

            line = a + b + c + d + e + f + g + h +   j + k + l + m + n + o + p + q + r + s + t + u + v + w + x + y + z + aa + ab + ac + ad + ae + af + ag + ah;

        } else if (SUBSTRING) {
            a = line.substr(0, 1);
            b = line.substr(1, 3);
            c = line.substr(4, 1);
            d = line.substr(5, 1);
            e = line.substr(6, 4);
            f = line.substr(10, 2);
            g = line.substr(12, 1);
            h = line.substr(13, 3);

            j = line.substr(16, 2);
            k = line.substr(18, 3);
            l = line.substr(21, 1);
            m = line.substr(22, 5);
            n = line.substr(27, 3);
            o = line.substr(30, 1);
            p = line.substr(31, 1);
            q = line.substr(32, 9);
            r = line.substr(41, 10);
            s = line.substr(51, 5);
            t = line.substr(56, 5);
            u = line.substr(61, 3);
            v = line.substr(64, 4);
            w = line.substr(68, 2);
            x = line.substr(70, 5);
            y = line.substr(75, 5);
            z = line.substr(80, 1);

            aa = line.substr(81, 3);
            ab = line.substr(84, 1);
            ac = line.substr(85, 1);
            ad = line.substr(86, 3);
            ae = line.substr(89, 4);
            af = line.substr(93, 30);
            ag = line.substr(123, 5);
            ah = line.substr(128, 4);

            line = a + b + c + d + e + f + g + h +   j + k + l + m + n + o + p + q + r + s + t + u + v + w + x + y + z + aa + ab + ac + ad + ae + af + ag + ah;
        }
        
        if (DEBUG) std::cout << "at " << records << " read (" << line << ")\n";
    }

    end = std::chrono::steady_clock::now();
    size_t dt_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

    fwd.close();
    std::cout  << records << "," << dt_ms << "\n";
}
