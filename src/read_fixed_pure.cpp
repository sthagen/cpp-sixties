#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

const size_t RECORD_BYTES = 132;

const bool DEBUG = (std::getenv("DEBUG_60S")) ? true : false;

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cerr << "Usage: " << __FILE__ << " database_path\n";
        exit(1);
    }
    const char *database_path{argv[1]};
    std::FILE *fwd = fopen(database_path, "r");
    if (fwd == nullptr) {
        std::cerr << "Error opening file " << database_path << "\n";
        exit(1);
    }
    char buf[RECORD_BYTES + 1];
    size_t records = 0;
    std::chrono::time_point<std::chrono::steady_clock> start, end;
    start = std::chrono::steady_clock::now();

    while (std::fread(&buf[0], sizeof buf[0], sizeof buf - sizeof buf[0], fwd) >= RECORD_BYTES) {
        buf[RECORD_BYTES] = '\0';
        ++records;
        if (DEBUG) std::cout << "at " << records << " read (" << buf << ")\n";
    }

    end = std::chrono::steady_clock::now();
    size_t dt_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

    fclose(fwd);
    std::cout  << records << "," << dt_ms << "\n";
}
