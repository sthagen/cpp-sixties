#! /bin/bash
# Execute the simple tests for different implementations on doubled corpora
pushd "$(dirname "${BASH_SOURCE[0]}")" || exit
corpora="a b c d e f g h i j k l m n o p q r s t"
corpus_missing=0
for x in ${corpora}
do
    (ls "data/${x}_pure.fwd" >> /dev/null 2>&1 && printf ".") || corpus_missing=1
done
printf "\n"
if [ "$corpus_missing" -eq 1 ] 
then
    printf "Rebuilding test data corpus ...\n"
    data/derive_corpus.sh
fi
for x in ${corpora}
do 
    echo "${x} read(warm chace), push_back, plus_equal, index_access, substring, pure_matrix:"
    f="data/${x}_unix.fwd"
    printf "read no op:   " && ./build/readline_unix "${f}"
    printf "push_back:    " && PUSH_BACK_60S=YES ./build/readline_unix "${f}"
    printf "plus_equal:   " && PLUS_EQUAL_60S=YES ./build/readline_unix "${f}"
    printf "index_access: " && INDEX_ACCESS_60S=YES ./build/readline_unix "${f}"
    printf "substr():     " && SUBSTRING_60S=YES ./build/readline_unix "${f}"
    f="data/${x}_pure.fwd"
    printf "matrix:       " && ./build/read_matrix_pure "${f}"
    echo "---"
done
popd || exit
printf "OK\n"
